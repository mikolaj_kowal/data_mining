import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from preds.mlp import fit as mlp_fit
from preds.svm import fit as svm_fit
from preds.knn import fit as knn_fit
from preds.knn import optimized_fit as opt_knn_fit
from preds.lr import fit as lr_fit
from preds.bayes import fit as bayes_fit
from preds.dtc import fit as dtc_fit
from preds.voting_hard import fit as hvoting_fit
from preds.voting_soft import fit as svoting_fit
from sklearn.metrics import confusion_matrix, classification_report
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.feature_selection import VarianceThreshold
from sklearn.metrics import f1_score


def split_data(filepath: str, feature_selector=None):
    content = pd.read_csv(filepath)
    X, y = content.drop(labels=["target"], axis=1), content["target"]
    if feature_selector:
        X = feature_selector.fit_transform(X, y)
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.2, random_state=42)
    return X_train, X_test, y_train, y_test


def display_report(name: str, y_test, preds):
    print(f'------------------ {name} ------------------------')
    print('\n\tConfusion matrix:')
    print(confusion_matrix(y_test, preds))
    score = f1_score(y_test, preds, average='weighted')
    print(f'Score: {score}')


def execute_for_data(X_train, X_test, y_train, y_test):
    pairs = [('Hard Voting', hvoting_fit)]
    pairs = [('MLP', mlp_fit), ('SVM', svm_fit), ('k-NN', knn_fit),
             ('DTC', dtc_fit), ('Bayes', bayes_fit), ('Logistic Regression', lr_fit),
             ('Hard Voting', hvoting_fit), ('Soft Voting', svoting_fit)]
    for pair in pairs:
        display_report(pair[0], y_test, pair[1](
            X_train, y_train).predict(X_test))
    opt_pairs = [('optimized k-NN', opt_knn_fit)]
    for pair in opt_pairs:
        display_report(pair[0], y_test, pair[1](
            X_train, y_train, X_test, y_test).predict(X_test))


def main():
    print('Standard')
    X_train, X_test, y_train, y_test = split_data('./data_dm/spam.dat')
    execute_for_data(X_train, X_test, y_train, y_test)
    print('\n\n\n\t300 features selection\n\n\n')
    X_train, X_test, y_train, y_test = split_data(
        './data_dm/spam.dat', SelectKBest(chi2, k=300))
    execute_for_data(X_train, X_test, y_train, y_test)
    print('\n\n\n\t80% treshold\n\n\n')
    X_train, X_test, y_train, y_test = split_data(
        './data_dm/spam.dat',  VarianceThreshold(threshold=(.8 * (1 - .8))))
    execute_for_data(X_train, X_test, y_train, y_test)


if __name__ == '__main__':
    main()
