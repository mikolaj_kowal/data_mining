from sklearn.ensemble import VotingClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC

def fit(X_train, y_train) -> VotingClassifier:
    voter = VotingClassifier(estimators=[('knn',KNeighborsClassifier(3)), ('mlp', MLPClassifier()), ('svc', SVC(gamma='auto'))], voting='hard')
    voter.fit(X_train, y_train)
    return voter