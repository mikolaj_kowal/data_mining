from sklearn.naive_bayes import MultinomialNB

def fit(X_train, y_train) -> MultinomialNB:
    bayes = MultinomialNB()
    bayes.fit(X_train, y_train)
    return bayes