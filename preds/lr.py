from sklearn.linear_model import LogisticRegression

def fit(X_train, y_train) -> LogisticRegression:
    lr = LogisticRegression(solver='lbfgs')
    lr.fit(X_train, y_train)
    return lr