from sklearn.tree import DecisionTreeClassifier

def fit(X_train, y_train) -> DecisionTreeClassifier:
    dtc = DecisionTreeClassifier()
    dtc.fit(X_train, y_train)
    return dtc