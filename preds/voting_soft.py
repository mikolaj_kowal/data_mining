from sklearn.ensemble import VotingClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.naive_bayes import MultinomialNB

def fit(X_train, y_train) -> VotingClassifier:
    voter = VotingClassifier(estimators=[('knn',KNeighborsClassifier(3)), ('mlp', MLPClassifier()), ('mnb', MultinomialNB())], voting='soft')
    voter.fit(X_train, y_train)
    return voter