from sklearn.neural_network import MLPClassifier

def fit(X_train, y_train) -> MLPClassifier:
    mlp = MLPClassifier()
    mlp.fit(X_train, y_train)
    return mlp