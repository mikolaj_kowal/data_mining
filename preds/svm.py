from sklearn.svm import SVC

def fit(X_train, y_train) -> SVC:
    svc = SVC(gamma='auto')
    svc.fit(X_train, y_train)
    return svc