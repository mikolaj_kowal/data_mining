from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import f1_score
from sklearn.metrics import confusion_matrix

def fit(X_train, y_train) -> KNeighborsClassifier:
    knn = KNeighborsClassifier(3)
    knn.fit(X_train, y_train)
    return knn

def optimized_fit(X_train, y_train, X_test, y_test) -> KNeighborsClassifier:
    best_score = 0
    best_nn = 1
    best_knn = None
    for i in range(best_nn, 20):
        knn = KNeighborsClassifier(i)
        print(knn.n_neighbors)
        knn.fit(X_train, y_train)
        preds = knn.predict(X_test)
        score = f1_score(y_test, preds, average='weighted')
        # print(confusion_matrix(y_test, preds))
        print(score, i)
        if score > best_score:
            best_score = score
            best_nn = i
            best_knn = knn
    print(f'Best knn with k={best_nn} with score={best_score}')
    return best_knn